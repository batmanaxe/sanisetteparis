#toilettes_paris


import streamlit as st
import pydeck as pdk

import pandas as pd
import requests
import json
from pandas.io.json import json_normalize
import json
import numpy as np

url_api="https://opendata.paris.fr/api/records/1.0/search/?dataset=sanisettesparis"

params = {'rows':900}
# Load the JSON to a Python list & dump it back out as formatted JSON


st.title('Toilettes publiques parisiennes')
st.markdown("Application affichant le statut des toilettes publiques parisiennes en temps réel")

@st.cache(persist=True)
def load_data(url_api):

    
    response = requests.get(url_api, params)
    content = response.content
    my_json = content
    data = json.loads(my_json)
    df = pd.json_normalize(data, 'records')
    drop_cols=['recordid','datasetid','geometry.type','geometry.coordinates','fields.geo_point_2d','fields.geo_shape.type']
    df = df.drop(drop_cols, axis=1)
    dict_replace={'fields.statut':'statut','fields.arrondissement':'arrondissement','fields.adresse':'adresse','fields.geo_shape.coordinates':'coordinates','fields.type':'type','fields.acces_pmr':'type','fields.url_fiche_equipement':'equipement','fields.relais_bebe':'relais_bebe',   }

    df = df.rename(columns=dict_replace)
    lat=[]
    lon=[]
    for i,row in df.iterrows():
        lon.append(df['coordinates'][i][0][0])
        lat.append(df['coordinates'][i][0][1])

    df['lat']=lat
    df['lon']=lon
    df = df.drop('coordinates', axis=1)
    df['record_timestamp']=pd.to_datetime(df['record_timestamp'])
    df =df.fillna('Non renseigné')
    df=pd.DataFrame(df)

    return df

data = load_data(url_api)
original_data=data
midpoint = (np.average(data['lon']),np.average(data['lat']))


if st.checkbox ('Show Raw Data',False):
	st.subheader('Raw Data')
	st.write(data)



select = st.selectbox('Statut',
	['Ouvert','Fermé','Non renseigné']
	)

data = data[data['statut']==select]

st.map(data[['lat','lon']])

r=(pdk.Deck(
    map_style="mapbox://styles/mapbox/light-v10",
    initial_view_state={
        "latitude": midpoint[1],
        "longitude": midpoint[0],
        "zoom": 11,
        "pitch": 50,
    },
    layers=[
        pdk.Layer(
        "Circle",
        data=original_data[['record_timestamp','lon','lat']],
        get_position=["lon", "lat"],
        auto_highlight=True,
        radius=50,
        extruded=True,
        pickable=True,
        elevation_scale=4,
        elevation_range=[0, 1000],
        ),
    ],
))





